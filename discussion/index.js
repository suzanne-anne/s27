/*
	What is a Data Model?

		A data model describes how data is organized and grouped in a database.

		By creating data models, we can anticipate which data will be manage by the Database Management System in accordance to the application to be developed.

	Data Modelling

		Database should have a purpose and its organization must be related to the kind of application we are building.

	Data Models
		- Blueprint for our document that we can follow and structure our data.
		- Show the relationships between our data.

	MongoDB Data Modeling Design

	- There are two ways in creating a MongoDB Data Model.
	
	=> Embedded Data Models
		-  generally known as "denormalize" models, and takes the advances of MongoDB's rich document (flexibility).
		- subdocument embedded inside another document.
		- Example:

		// User Document
		{
			"id": 1,
			"name": "John Doe",
			'age': 25,
			"job": "Web Developer",
			address:[
				{
					"city": "Manila",
					"country": "Philippines"
				},
				{
					"city": "Pampanga",
					"country": "Philippines"
				}
			]
		}

	=> Reference Data Models
		- This are known as "normalize" data models and describes the relationship using references (id) between documents.
		- Uses "document ID" to connect a document to another document.
		- Example:

		//User Collection
		{
			"id": 1,
			"name": "John Doe",
			'age': 25,
			"job": "Web Developer",
		}

		//Address Collection
		[
			{
				"id": 1,
				"city": "Manila"
				"country": "Philippines"
				"user_id": 1
			},
			{
				"id": 2,
				"city": "Pampanga"
				"country": "Philippines"
				"user_id": 1
			}
		]
*/

/*
	Model Relationship

	- To be able to properly organize an application database should also identify the relationship between our models.

*/

// One to One (1:1) - The relationship means that a model is exclusively related to only one model.

// Using Reference Data Models
// Employee
{
	"id":"dev-2023",
	"firstName": "Jane",
	"lastName": "Smith",
	"email": "janesmith@mail.com"
}

// Credentials
{
	"id":"cred-001",
	"employee_id":"dev-2023", //reference id
	"role":"Web Developer"
	"team":"Tech"
}

/*
	Embedded Data models
	
	- Embedding - putting a document inside another document.
	- Subdocuments - are documents embedded in a parent document.

*/

// Employee
{
	"id":"dev-2023",
	"firstName": "Jane",
	"lastName": "Smith",
	"email": "janesmith@mail.com",
	"credentials: "{
		"id":"cred-001",
		"role":"Web Developer"
		"team":"Tech"
	}
}

/*
	One to Many (1:m)

	- One model is related to multiple other models, however the other models are only related to one.

	Person - many email address

	Email address - One person

	Blog Post - many comments

		A blog post can have multiple comments, but each comment only belong to a single post.
*/

// Reference Data Model

// Blog
{
	"id":"blog-001"
	"title":"This is an Awesome Blog!",
	"content": "This is an awesome blog that I created!",
	"createdOn":"4/18/2023",
	"author":"blogWriter01"
}

// Comments
{
	"id":"blogcomment1",
	"comment": "Awesome Blog!",
	"author": "blogwriter1",
	"blog_id": "blog-001"
},
{
	"id":"blogcomment2",
	"comment": "Meh. Not awesome at all.",
	"author": "notHater22",
	"blog_id": "blog-001"
}

/*
	Embedded Data Models
	
	-Subdocument Array - an array of subdocuments per single parent document.

*/

//Blog
{

	"id": "blog-001",
	"title": "This is an Awesome Blog!",
	"content": "This is an awesome blog that I created!",
	"createdOn": "4/18/2023",
	"author": "blogwriter1",
	"comments": [
		{
			"id":"blogcomment1",
			"comment": "Awesome Blog!",
			"author": "blogwriter1"
		},
		{
			"id":"blogcomment2",
			"comment": "Meh. Not awesome at all.",
			"author": "notHater22"
		}
	]
}

/*

	Many to Many (m:m)

	- Multiple document are related to multiple documents.

		users <-> course

	- When a many to many relationship is created, an "associative entity/model" is created.
		- Associative entity is a model that breaks down m:m to 1:m relationship.

		user -> enrollments/transactions/enrollees <- course

*/

// Reference Data Models

// User Collection
{
	id, //unique identifier for the document
	firstName- string,
	lastName- string,
	email- string,
	password - string,
	mobileNumber - string,
	isAdmin - boolean
}

// Course Collection 
{
	id,
	name - string,
	description - string,
	price - number,
	slots - number,
	isActive - boolean
}

// Transaction Collection (Associative Entity)
{
	id,
	user_id, // the unique identifier from user collection
	course_id, // the unique indentifier from course collection
	isPaid - boolean,
	paymentMethod-string,
	dateEnrolled - DateTime
}

/*
	Using the "Embedded data models", m:m can also be expressed in another way.
	
	- Two Way Embedding (Subdocument Arrays) - The associative array is places inside of the both models/documents.
*/

// User collection
{
	id, //unique identifier for the document
	firstName- string,
	lastName- string,
	email- string,
	password - string,
	mobileNumber - string,
	isAdmin - boolean,
	//transaction model (associative entity) is embedded inside the user collection
	enrollments:[ 
		{
			id,
			course_id, //the unique identifier for the course
			courseName, //optional
			isPaid,
			paymentMethod,
			dateEnrolled
		}
	]
}

// Course Collection
{
	id,
	name - string,
	description - string,
	price - number,
	slots - number,
	isActive - boolean,
	//transaction model (associative entity) is embedded inside the course collection
	enrollees:[
		{
			id,
			user_id,
			email, //optional
			isPaid,
			paymentMethod,
			dateEnrolled
		}
	]
}

// Sample Mock data for m:m relationship using reference data model

// User Collection
{
	"id":"stdnt-01",
	"firstName": "John",
	"lastName": "Doe",
	"email": "johndoe@mail.com",
	"password": "john123", // encrypted
	"mobileNumber": "09123456789",
	"isAdmin": false
}

// Course Collection 
{
	"id":"course-01",
	"name": "HTML",
	"description": "Intro to Web Development",
	"price": 2000,
	"slots": 25,
	"isActive": true
}

// Transaction Collection (Associative Entity)
{
	"id": "trans-001",
	"user_id": "stdnt-01", 
	"course_id": "course-01",
	"isPaid":true,
	"paymentMethod": "CreditCard"
	"dateEnrolled":"04/18/2023 7:37 PM"
}